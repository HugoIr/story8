from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from .views import accordion

# Create your tests here.

class AccordionUnitTest(TestCase):
    
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_statusan_using_statusan_func(self):
        got = resolve('/')
        self.assertEqual(got.func, accordion)
    

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import time
class AccordionFunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options= webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super(AccordionFunctionalTest, self).tearDown()
    

    # Test 1
    # find items elements and click it
    # Check the dropdown is clickable by assertTrue for the existence of itemData
    def testclickOn(self):
        selenium = self.selenium

        # Open url target
        openUrl = selenium.get(self.live_server_url + '/')


        for i in range(1,5):
            item = selenium.find_element_by_id('item' + str(i)).click()
            itemData = selenium.find_element_by_id('itemdata' + str(i))
            self.assertTrue(bool(itemData))

        # Test 4
        # find "Change Background" button and click it
        # check if the color is changed to the next color

        # Item List for test below
        bgList = [
        '1.jpg',
        '2.jpg',
        '3.jpg',
        '4.jpg',
        ]
        colorList = [
        "rgba(0, 128, 0, 1)",  #  "green"
        "rgba(0, 0, 255, 1)",  #  "blue"
        "rgba(255, 0, 0, 1)",  #  "red"
        "rgba(165, 42, 42, 1)",  #  "brown"
        ]

        dataColorList = [
        "rgba(245, 245, 245, 1)",  #  "WhiteSmoke"
        "rgba(173, 216, 230, 1)",  #  "LightBlue"
        "rgba(255, 235, 205, 1)",  #  "BlanchedAlmond"
        "rgba(222, 184, 135, 1)",  #  "BurlyWood"
        ]

        for i in range(0,4):
            selenium.find_element_by_id("bg_btn").click()    
            body = selenium.find_element_by_id('body')
            bg = body.value_of_css_property('background-image')     #print =url("http://localhost:xxxxx/static/img/x.jpg")
            # test background img url 
            # print(bgList[i])
            time.sleep(3)
            assert 'static/img/'+bgList[i] in bg

            # test block item color
            blockItem = selenium.find_element_by_class_name('block-item')
            blockItemCss = blockItem.value_of_css_property('background-color')
            self.assertEqual(blockItemCss, colorList[i])

            # test item-data color
            theItemData = selenium.find_element_by_class_name('item-data')
            theItemDataCss = theItemData.value_of_css_property('background-color')
            self.assertEqual(theItemDataCss, dataColorList[i])


    # Test 2
    # Test up button of each item
    def testPositionAfterClickUpButton(self):
        selenium = self.selenium

        # Open url target
        openUrl = selenium.get(self.live_server_url + '/')

        # Test item 1, it is expected for not moving
        upButton = selenium.find_element_by_id('a1')
        yLocation = upButton.location['y']
        upButton.click()
        yLocationAfter = upButton.location['y']
        self.assertEqual(yLocation - yLocationAfter, 0)        # If the item is clicked, it'll move by 0 unit on y-axis

        # Test other items, it is expected to be moved
        for i in range(2,5):
            upButton = selenium.find_element_by_id('a' + str(i))
            yLocation = upButton.location['y']
            upButton.click()
            yLocationAfter = upButton.location['y']
            self.assertEqual(yLocation - yLocationAfter, 40)        # If the item move, it'll move by 40 unit on y-axis
        

    # Test 3
    # Test down button of each item
    def testPositionAfterClickDownButton(self):
        selenium = self.selenium

        # Open url target
        openUrl = selenium.get(self.live_server_url + '/')

        # Test item 4, it is expected for not moving
        upButton = selenium.find_element_by_id('b4')
        yLocation = upButton.location['y']
        upButton.click()
        yLocationAfter = upButton.location['y']
        self.assertEqual(yLocation - yLocationAfter, 0)        # If the item is clicked, it'll move by 0 unit on y-axis

        # Test other items, it is expected to be moved
        for i in range(1,4):
            upButton = selenium.find_element_by_id('b' + str(i))
            yLocation = upButton.location['y']
            upButton.click()
            yLocationAfter = upButton.location['y']
            self.assertEqual(yLocationAfter - yLocation, 40)        # If the item move, it'll move by 40 unit on y-axis



