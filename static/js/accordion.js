$(document).ready(function(e){
  $('.tombol-bawah').click(function(e){
      var itemId = $(this).parents().eq(1).attr('id');
      var nextItemId = $(this).parents().eq(1).next('section').attr('id');
      $("#" + itemId).before($("#" + nextItemId));
  });
});

$(document).ready(function(e){
  $('.tombol-atas').click(function(e){
      var itemId = $(this).parents().eq(1).attr('id');
      var prevItemId = $(this).parents().eq(1).prev('section').attr('id');
      $("#" + prevItemId).before($("#" + itemId));
  });
});

$(document).ready(function(e){
  $('.item').click(function (e){
    // alert($(this).parent('div').next('.item-data').attr("id"));
    if ($(this).parent('div').next('.item-data').css('display') != 'block'){
        // alert($(this).parent('div').next('.item-data').attr("id"))
        $('.active').slideUp('fast').removeClass('active');
        $(this).parent('div').next('.item-data').addClass('active').slideDown('fast');
        
    } 
    else {
      $('.active').slideUp('fast').removeClass('active');
    }
  });
});

var colorList = [
  "green",
  "blue",
  "red",
  "brown",
]

var dataColorList = [
  "WhiteSmoke",
  "LightBlue",
  "BlanchedAlmond",
  "BurlyWood",
]

var bgList = [
  '1.jpg',
  '2.jpg',
  '3.jpg',
  '4.jpg',
  
]
var counter = 0;
$(document).ready(function(e) {
  $("#bg_btn").click(function(e){

    if(counter >= bgList.length){
      counter = 0;
    }
    $('body').css('background', "url(static/img/" + bgList[counter] + ")");
    $(".block-item").css("background", colorList[counter]);
    $(".item-data").css("background", dataColorList[counter]);
    counter = counter + 1

    });

  });
